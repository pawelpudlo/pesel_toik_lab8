package com.demo.springboot.src;

public class CheckPeselImpl implements CheckPesel {

    private String Pesel = "";

    public CheckPeselImpl(String pesel) {
        Pesel = pesel;
    }

    @Override
    public int checkPeselNumber() {

        if(this.Pesel.equals("") || this.Pesel.length() !=11){
            return 0;
        }


        String day = this.Pesel.substring(4,6);

        if(Integer.parseInt(day) > 31 ) return 0;



        int control = Integer.parseInt(this.Pesel.substring(0,1)) +
                3 * Integer.parseInt(this.Pesel.substring(1,2))
                + 7 * Integer.parseInt(this.Pesel.substring(2,3))
                + 9 * Integer.parseInt(this.Pesel.substring(3,4))
                + Integer.parseInt(this.Pesel.substring(4,5))
                + 3 * Integer.parseInt(this.Pesel.substring(5,6))
                + 7 * Integer.parseInt(this.Pesel.substring(6,7))
                + 9 * Integer.parseInt(this.Pesel.substring(7,8))
                + Integer.parseInt(this.Pesel.substring(8,9))
                + 3 * Integer.parseInt(this.Pesel.substring(9,10))
                + Integer.parseInt(this.Pesel.substring(10));


        if( control % 10 == 0) return 1;


        return 0;
    }

    public String getPesel() {
        return Pesel;
    }

    public void setPesel(String pesel) {
        Pesel = pesel;
    }
}
